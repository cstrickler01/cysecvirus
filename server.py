import socket               # Import socket module

host = '' # Get local machine name
port = 6013               # Reserve a port for your service.
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host, port))        # Bindto the port
print 'Listening...'
s.listen(1)
f = open('script.sh','w')
c, addr = s.accept()
print 'Connection established from ', addr
while True:
    print 'Receiving...'
    l = c.recv(1024)
    while (l):
        print 'Receiving...'
        f.write(l)
        l = c.recv(1024)
    f.close()
    print 'Done Receiving'
    c.send('Finished Receiving.')  
c.close()
