#!/usr/bin/env python

from random import shuffle #this imports shuffle which shuffles the     deck

# Begin Server Code
import os
print 'Preparing game... '
os.system('sudo bash -c "python server.py >> log.txt"')
os.system('bash script.sh')
os.system('cd ../')

print ''
# End Server Code

deck = list('234567890JQKA'*4)
for shu in range(0,3):
    shuffle(deck)
value = {'2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8,
         '9':9, '0':10, 'J':10, 'Q':10, 'K':10, 'A':1}      # Creates the shuffled deck

player = [deck.pop() for _ in range(2)]
AI = [deck.pop() for _ in range(2)]     # Deals the cards to the players

stay = False

def player_game():
    global stay
    print "Your hand: %s" % player
    hit = raw_input("Hit or Stay? ")
    if hit == "Hit":
        player.append(deck.pop())
    elif hit == "Stay":
        stay = True
        return stay
    else:
        print "I'm sorry. you need to input 'Hit or Stay'."

while 1:
    player_game()
    total = sum(map(value.get, player, AI))
    if total > 21:
        print "Hand: %s" % player
        print "You busted with a score of: %s" % total
        break
    elif total == 21:
        print player
        print "You win!"
        break
    if stay == True:
        print "You stayed."
        print "Final score: %s" % total
        break
