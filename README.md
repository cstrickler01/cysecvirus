# CySecVirus
## 22 February 2019

This repository holds virus scripts to be run on a client machine (Kali Linux) and a server machine (Metasploitable 2).

The blackjack.py file acts as the deliverable for the virus, and calls the server.py file. The server.py file establishes a connection with the client machine, who responds by running client.py.

Once this is complete, the client sends the script.sh file to the server. Script.sh will then install itself as a boot operation, and move itself to the root directory.

I didn't add any malicious calls, as these scripts together infiltrate the system and put themselves in a position to do harm.
